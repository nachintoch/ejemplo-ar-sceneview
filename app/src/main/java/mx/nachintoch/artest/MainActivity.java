package mx.nachintoch.artest;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.PixelCopy;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.ar.core.AugmentedFace;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.Sceneform;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.rendering.RenderableInstance;
import com.google.ar.sceneform.rendering.Texture;
import com.google.ar.sceneform.ux.ArFrontFacingFragment;
import com.google.ar.sceneform.ux.AugmentedFaceNode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * https://github.com/SceneView/sceneform-android
 */
public class MainActivity extends AppCompatActivity {

    private ArFrontFacingFragment fragmentoRostrosRa;
    private ArSceneView vistaEscenaRa;

    private Texture texturaRostro;

    private ModelRenderable modeloRostro;

    private final Set<CompletableFuture<?>> cargadores = new HashSet<>();

    private final HashMap<AugmentedFace, AugmentedFaceNode> nodosRostros = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().addFragmentOnAttachListener(this::onAttachFragment);
        if (savedInstanceState == null) {
            if (Sceneform.isSupported(this)) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.vista_ar_fl, ArFrontFacingFragment.class, null)
                        .commit();
            }
        }
        ImageButton botonCapturar = findViewById(R.id.capturar_button);
        botonCapturar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeScreenshot();
            }
        });
        loadTextures();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelaCargadores();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_texture_cb) {
            cambiaOpcionAr();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onAttachFragment(@NonNull FragmentManager fragmentManager,
                                 @NonNull Fragment fragment) {
        if (fragment.getId() == R.id.vista_ar_fl) {
            fragmentoRostrosRa = (ArFrontFacingFragment) fragment;
            fragmentoRostrosRa.setOnViewCreatedListener(this::onViewCreated);
        }
    }

    public void onViewCreated(ArSceneView arSceneView) {
        vistaEscenaRa = arSceneView;
        // This is important to make sure that the camera stream renders first so that
        // the face mesh occlusion works correctly.
        arSceneView.setCameraStreamRenderPriority(Renderable.RENDER_PRIORITY_FIRST);
        // Check for face detections
        fragmentoRostrosRa.setOnAugmentedFaceUpdateListener(this::onAugmentedFaceTrackingUpdate);
    }

    public void onAugmentedFaceTrackingUpdate(AugmentedFace rostroRa) {
        Log.d(MainActivity.class.getSimpleName(),
                String.format("Textura aumentable: %b", texturaRostro != null));
        Log.d(MainActivity.class.getSimpleName(),
                String.format("Rendereable aumentable: %b", modeloRostro != null));
        if (texturaRostro == null && modeloRostro == null) {
            return;
        }
        AugmentedFaceNode nodoRostroExistente = nodosRostros.get(rostroRa);
        switch (rostroRa.getTrackingState()) {
            case TRACKING:
                if (nodoRostroExistente == null) {
                    if (texturaRostro != null) {
                        aumentaTextura(rostroRa);
                    } else { // textura NULL -> modelo NO NULL
                        aumentaModelo(rostroRa);
                    }// aumenta textura OR modelo según activo
                }
                break;
            case STOPPED:
                if (nodoRostroExistente != null) {
                    vistaEscenaRa.getScene().removeChild(nodoRostroExistente);
                }
                nodosRostros.remove(rostroRa);
                break;
        }
    }

    private void cambiaOpcionAr() {
        if (texturaRostro == null && modeloRostro == null) {
            return;
        }
        cancelaCargadores();
        for (AugmentedFace rostroAumentado : nodosRostros.keySet()) {
            AugmentedFaceNode nodoRostroAr = nodosRostros.remove(rostroAumentado);
            vistaEscenaRa.getScene().removeChild(nodoRostroAr);
        }
        if (texturaRostro != null) {
            texturaRostro = null;
            loadModels();
        } else {
            modeloRostro = null;
            loadTextures();
        }
    }

    private void cancelaCargadores() {
        for (CompletableFuture<?> cargador : cargadores) {
            if (!cargador.isDone()) {
                cargador.cancel(true);
            }
        }
    }

    private void loadModels() {
        cargadores.add(ModelRenderable.builder()
                .setSource(this, Uri.parse("fox.glb"))
                .setIsFilamentGltf(true)
                .build()
                .thenAccept(model -> modeloRostro = model)
                .exceptionally(throwable -> {
                    Toast.makeText(this, "No pudo cargarse el modelo",
                            Toast.LENGTH_LONG).show();
                    return null;
                }));
    }

    private void aumentaTextura(AugmentedFace rostroRa) {
        AugmentedFaceNode nodoRostro = new AugmentedFaceNode(rostroRa);
        nodoRostro.setFaceMeshTexture(texturaRostro);
        vistaEscenaRa.getScene().addChild(nodoRostro);
        nodosRostros.put(rostroRa, nodoRostro);
    }

    private void aumentaModelo(AugmentedFace rostroRa) {
        AugmentedFaceNode nodoRostro = new AugmentedFaceNode(rostroRa);
        RenderableInstance modelo
                = nodoRostro.setFaceRegionsRenderable(modeloRostro);
        modelo.setShadowCaster(false);
        modelo.setShadowReceiver(true);
        vistaEscenaRa.getScene().addChild(nodoRostro);
        nodosRostros.put(rostroRa, nodoRostro);
    }

    private void loadTextures() {
        cargadores.add(Texture.builder()
                .setSource(this, Uri.parse("freckles.png"))
                .setUsage(Texture.Usage.COLOR_MAP)
                .build()
                .thenAccept(texture -> texturaRostro = texture)
                .exceptionally(throwable ->  {
                    Toast.makeText(this, "No es posible cargar la textura",
                            Toast.LENGTH_LONG).show();
                    return null;
                }));
        Log.d(MainActivity.class.getSimpleName(),
                String.format("Texturas inicializadas: %d", cargadores.size()));
    }

    private void makeScreenshot() {
        final Bitmap bitmap = Bitmap.createBitmap(
                vistaEscenaRa.getWidth(), vistaEscenaRa.getHeight(), Bitmap.Config.ARGB_8888);
        final HandlerThread handlerThread = new HandlerThread("PixelCopier");
        handlerThread.start();
        PixelCopy.request(vistaEscenaRa, bitmap, (copyResult) -> {
            if (copyResult == PixelCopy.SUCCESS) {
                try {
                    saveBitmapLocally(bitmap);
                } catch(IOException e) {
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                Toast.makeText(MainActivity.this,
                        "Captura guardada", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this,
                        "No se pudo capturar la pantalla", Toast.LENGTH_SHORT).show();
            }
            handlerThread.quitSafely();
        }, new Handler(handlerThread.getLooper()));
    }

    private void saveBitmapLocally(Bitmap bitmap) throws IOException {
        File dirDestino = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES) + "/arTestScreenshots");
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss",
                Locale.getDefault());
        String fechaFormateada = sdf.format(c.getTime());
        File archivoDest = new File(dirDestino, "capturaAr_" + fechaFormateada + ".jpg");
        Files.createDirectories(dirDestino.toPath());
        FileOutputStream fileOutputStream = new FileOutputStream(archivoDest);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}